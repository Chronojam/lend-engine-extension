// let thing = "https://things.sharebristol.org.uk/admin/item/1704";
// https://things.sharebristol.org.uk/admin/maintenance/item-1704/plan-4

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
const days   = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

let baseUrl = "https://things.sharebristol.org.uk/admin/maintenance"
let planIds = [
  "plan-4", // Function Test
  "plan-2", // Clean
  "plan-5", // Photography
  "plan-6", // User Guide
  "plan-7", // PPE Noted
  "plan-11", // Thing/Box casing
]

async function scheduleStuff() {
  // Get the page url first.
  let url = await getPage()
  // Then we want the item id.
  // Urls are assumed to be: https://things.sharebristol.org.uk/admin/item/1704
  let parts = url.split('/')
  // So we just take the final part and move on with our lives.
  let id = parts[parts.length - 1]

  planIds.forEach(plan => {
    scheduleInitial(`item-${id}`, plan);
  })
}

async function getPage(){
  return browser.tabs.query({currentWindow: true, active: true})
    .then((tabs) => {
      return tabs[0].url;
    })
}

// This schedules 1 single check.
async function scheduleInitial(itemNumber, planId) {
  const url = `${baseUrl}/${itemNumber}/${planId}`

  // The date format is the following: Mon+Dec+4+2023
  const date = new Date();
  const now  = `${days[date.getDay()]}+${months[date.getMonth()]}+${date.getDate()}+${date.getFullYear()}`;
  console.log(`maint date: ${now}`);
  let response = await fetch(url, {
    method: "POST",
    credentials: "include",
    mode: "cors",
    body: `id=&maintenance_date=${now}&moveToLocation=&note=`,
    headers: {
      "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/116.0",
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
      "Accept-Language": "en-US,en;q=0.5",
      "Content-Type": "application/x-www-form-urlencoded",
      "Upgrade-Insecure-Requests": "1",
      "Sec-Fetch-Dest": "document",
      "Sec-Fetch-Mode": "navigate",
      "Sec-Fetch-Site": "same-origin",
      "Sec-Fetch-User": "?1"
    }
  });
}

browser.browserAction.onClicked.addListener(scheduleStuff);

console.log("loading addon!");